
var nav_toggled = false;

$( document ).ready(function(){
  var pipe_el = $('<div id="float-pipe"/></div>');
  var size_el = $('nav');
  var bind_to_el = $('div.wy-grid-for-nav');

  var showPipe = function(y_position) {
    console.log('show pipe')
    console.log(size_el.css('width'))

    pipe_el.css('width', size_el.css('width'));
    pipe_el.css('top', y_position).fadeIn();
  }

  var hidePipe = function() {
    pipe_el.fadeOut(0);
  }

  var isResponsive = function() {
    return $(document).width() < 754 || $(window).height() < 400
  }

  $('i.fa,.fa-bars').click(function() {
    if (nav_toggled){
      hidePipe();
      nav_toggled = false;
    }
    else{
      setTimeout(function() {
        showPipe($(window).height() - 120)
      }, 500);
      nav_toggled = true;
    }
  });

  if (!isResponsive()){
    // Have to wait for some divs to render final heights
    setTimeout(function() {
      showPipe( 50 + $('footer').position('top')['top'])
    }, 1000);
  }

  bind_to_el.prepend(pipe_el);

})
